import puppeteer from "puppeteer";

export class MegaDownloader {
  private urls: Array<string> = [];
  private folderPath: string = "";
  constructor(urls: Array<string>, folderPath: string) {
    this.folderPath = folderPath;
    this.urls = urls;
  }

  private async download(page: puppeteer.Page, url: string) {
    await page.goto(url);
    await page.waitFor(2000);
    // from firefox inspector select button , right click to select copy > css selector
    const downloadButtonSelector =
      ".video-mode-wrapper > div:nth-child(2) > div:nth-child(1)";

    await page.waitForSelector(downloadButtonSelector, {
      visible: true
    });
    /*const sizeSelector =
      "div.info-block:nth-child(2) > div:nth-child(3) > div:nth-child(2) > span:nth-child(3)";
    const elemSize = await page.$(sizeSelector);
    if (elemSize) {
      console.log("the size is ", elemSize.toString());
    }*/

    // await page.waitFor(4000);
    await page.click(downloadButtonSelector);

    const completedDownloadSelector =
      ".main-transfer-info > span:nth-child(3) > span:nth-child(2)";
    await page.waitForSelector(completedDownloadSelector, {
      visible: true,
      timeout: 0
    });
    await page.waitFor(10000);
  }

  public async startDownloading() {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();

    const session = await page.target().createCDPSession();
    await session.send("Page.setDownloadBehavior", {
      behavior: "allow",
      downloadPath: this.folderPath
    });
    for (const url of this.urls) {
      console.log("Downloading ", url);
      await this.download(page, url);
      console.log("Finished with ", url);
    }
    await browser.close();
  }
}

export async function downloadFromMega(url: string, folderPath: string) {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  const session = await page.target().createCDPSession();
  await session.send("Page.setDownloadBehavior", {
    behavior: "allow",
    downloadPath: folderPath
  });

  await page.goto(url);
  // from firefox inspector select button , right click to select copy > css selector
  const downloadButtonSelector =
    ".video-mode-wrapper > div:nth-child(2) > div:nth-child(1)";

  await page.waitForSelector(downloadButtonSelector, {
    visible: true
  });
  const sizeSelector =
    "div.info-block:nth-child(2) > div:nth-child(3) > div:nth-child(2) > span:nth-child(3)";
  const elemSize = await page.$(sizeSelector);
  if (elemSize) {
    console.log("the size is ", elemSize.toString());
  }

  // await page.waitFor(4000);
  await page.click(downloadButtonSelector);

  const completedDownloadSelector =
    ".main-transfer-info > span:nth-child(3) > span:nth-child(2)";
  await page.waitForSelector(completedDownloadSelector, {
    visible: true,
    timeout: 0
  });
  await page.waitFor(10000);
  await browser.close();
}
