import * as fileSrv from "./fileSrv";
import * as browserSrv from "./browserSrv";
import cmder from "commander";

async function main(list: string, folder: string) {
  console.log(
    "Will download from this list ",
    list,
    " and save the files to this path ",
    folder
  );
  const urls = await fileSrv.getList(list);
  const md = new browserSrv.MegaDownloader(urls, folder);
  console.log("Starting downloading the links");
  await md.startDownloading();
  console.log("Finished downloading the links");
}

cmder
  .option("-l, --list <dir>", "List of mega links seperated by new line")
  .option("-f, --folder <dir>", "Path to donload files in the folder.")
  .parse(process.argv);

if (!cmder.list) {
  console.log("Error: It is missing the text file for the list.");
  process.exit(1);
}

if (!cmder.folder) {
  console.log(
    "Error: It is missing the path of the folder that will save the files there."
  );
  process.exit(1);
}
main(cmder.list, cmder.folder);
