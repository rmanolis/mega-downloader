import * as fs from "fs";
import * as util from "util";

export async function getList(file: string): Promise<string[]> {
  const readFile = util.promisify(fs.readFile);
  const buf = await readFile(file);
  const text = buf.toString();
  const lines = text.split("\n");
  return lines;
}
